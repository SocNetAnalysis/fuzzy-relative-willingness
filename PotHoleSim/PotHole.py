import logging
import soil
import random

'''This class written for simulating PotHole with global values of $P_i$ and $P_e$'''
class PotHole(soil.agents.FSM):
    
    @soil.agents.default_state
    @soil.agents.state
    def idle(self):
        r = random.random()
        if self['has_ext'] and r < self.env['prob_tv_spread']:
            self.src = -1
            self.tag = "#Football"
            return self.tweet
        return
    
    @soil.agents.state
    def tweet(self):
        self.state['tag'] = self.tag
        self.state['src'] = self.src
        for neighbor in self.get_neighboring_agents():
            neighbor.tag = self.tag
            neighbor.src = self.id
            neighbor.state['id'] = self.tweet_recv.id
        return self.idle
    
    @soil.agents.state
    def tweet_recv(self):
        prob_infect = self.env['prob_neighbor_spread']
        r = random.random()
        if r < prob_infect:            
            return self.tweet
        return self.idle
    
'''This class written for simulating PotHole with global values of $P_i$ but fixed values of $P_e$'''    
class PotHoleOne(soil.agents.FSM):
    
    @soil.agents.default_state
    @soil.agents.state
    def idle(self):
        r = random.random()
        if self['has_ext'] and r < self['prob_tv_spread']:
            self.src = -1
            self.tag = "#Football"
            return self.tweet
        return
    
    @soil.agents.state
    def tweet(self):
        self.state['tag'] = self.tag
        self.state['src'] = self.src
        for neighbor in self.get_neighboring_agents():
            neighbor.tag = self.tag
            neighbor.src = self.id
            neighbor.state['id'] = self.tweet_recv.id
        return self.idle
    
    @soil.agents.state
    def tweet_recv(self):
        prob_infect = self.env['prob_neighbor_spread']
        r = random.random()
        if r < prob_infect:            
            return self.tweet
        return self.idle
    

'''This class written for simulating PotHole with global values of $P_e$ but fixed values of $P_i$'''    
class PotHoleTwo(soil.agents.FSM):
    
    @soil.agents.default_state
    @soil.agents.state
    def idle(self):
        r = random.random()
        if self['has_ext'] and r < self.env['prob_tv_spread']:
            self.src = -1
            self.tag = "#Football"
            return self.tweet
        return
    
    @soil.agents.state
    def tweet(self):
        self.state['tag'] = self.tag
        self.state['src'] = self.src
        for neighbor in self.get_neighboring_agents():
            neighbor.tag = self.tag
            neighbor.src = self.id
            neighbor.state['id'] = self.tweet_recv.id
        return self.idle
    
    @soil.agents.state
    def tweet_recv(self):
        prob_infect = self['prob_neighbor_spread']
        r = random.random()
        if r < prob_infect:            
            return self.tweet
        return self.idle

'''Controling the max cascade size'''
class PotHoleThree(soil.agents.FSM):
    
    @soil.agents.default_state
    @soil.agents.state
    def idle(self):
        r = random.random()
        if self['has_ext'] and r < self.env['prob_tv_spread']:
            self.src = -1
            self.tag = "#Football"
            self.cascade = 0
            return self.tweet
        return
    
    @soil.agents.state
    def tweet(self):
        self.state['tag'] = self.tag
        self.state['src'] = self.src
        if self.cascade < self.env['max_cascade_size']:
            for neighbor in self.get_neighboring_agents():
                neighbor.tag = self.tag
                neighbor.cascade = self.cascade + 1
                neighbor.src = self.id
                neighbor.state['id'] = self.tweet_recv.id
        return self.idle
    
    @soil.agents.state
    def tweet_recv(self):
        prob_infect = self.env['prob_neighbor_spread']
        r = random.random()
        if r < prob_infect:            
            return self.tweet
        return self.idle