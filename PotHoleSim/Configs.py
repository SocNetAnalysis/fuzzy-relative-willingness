from PotHoleSim import PotHole as ph

def get_config(type, name, net_file, env_params, states = None):
    if(type == 'both_vary'):
        return { 
            'name': name, 'max_time': 2000, 'interval': 1, 'num_trials': 1, 
            'network_params': { 'path': net_file },
            'network_agents': [{ 'agent_type': ph.PotHole, 'weight': 9, 'state': { 'has_ext': 0 }},
                               { 'agent_type': ph.PotHole, 'weight': 1, 'state': {'has_ext' : 1 }}],
            'environment_agents': [], 'environment_params': env_params
               }
    if(type == 'vary_p_e'):
        return { 
            'name': name, 'max_time': 2000, 'interval': 1, 'num_trials': 1, 'agent_type': ph.PotHoleOne,
            'default_state': {'has_ext':0, 'prob_tv_spread':0.0}, 'network_params': { 'path': net_file},
            'states': states, 'environment_params': env_params
               }
    if(type == 'vary_p_i'):
        return {
            'name': name, 'max_time': 2000, 'interval': 1, 'num_trials': 1, 
            'network_params': { 'path': net_file},
            'network_agents': [{ 'agent_type': ph.PotHoleTwo, 'weight': 9, 'state': { 'has_ext': 0 }},
                               { 'agent_type': ph.PotHoleTwo, 'weight': 1, 'state': {'has_ext' : 1 }}],
            'states': states, 'environment_params': env_params
                }
    if(type == 'cascade_size'):
        return { 
            'name': name, 'max_time': 2000, 'interval': 1, 'num_trials': 1, 
            'network_params': { 'path': net_file }, 
            'network_agents': [{ 'agent_type': ph.PotHoleThree, 'weight': 9, 'state': { 'has_ext': 0 }},
                               { 'agent_type': ph.PotHoleThree, 'weight': 1, 'state': {'has_ext' : 1 }}],
            'environment_params': env_params
            }
        
        
        