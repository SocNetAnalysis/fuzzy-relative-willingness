There are three Jupyter Notebook in the repository containing all the code used for the paper. 

## Code:

- File: FRW_Simulation.ipynb

Contians all the code needed for the simmulations.

- File: FRW_Twitter.ipynb

Contains code for the twitter data

- File: FRW_Wiebo.ipynb

Contains code for the Weibo data

## Data Sets:

Synthetic datasets are in the dataset folder.

Twitter data can be downloaded from 

http://carl.cs.indiana.edu/data/#virality2013

Weibo data is available at 

https://aminer.org/influencelocality